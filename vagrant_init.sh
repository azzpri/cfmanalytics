#!/usr/bin/env bash

apt-get update
apt-get -y upgrade
apt-get install -y python3-pip
apt-get install build-essential libssl-dev libffi-dev python3-dev
apt-get install -y python3-venv
mkdir /cfmanalytics
chown -R ubuntu:ubuntu /cfmanalytics
cd /cfmanalytics/
pyvenv venv
source venv/bin/activate
pip3 install --upgrade pip
pip3 install django

apt-get -y install nodejs && apt-get -y install npm
ln -s /usr/bin/nodejs /usr/bin/node
npm cache clean -f
npm install -g n
n stable
ln -s /vagrant/ /cfmanalytics/site
cd /cfmanalytics/site
npm install

