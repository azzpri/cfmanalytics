from django.contrib import admin
from .models import Profile

class ProfileAdmin(admin.ModelAdmin):
    model = Profile
    list_display = ('username', 'facebook_ads')

    def username(self, obj):
        return "%s %s" %(obj.user.first_name, obj.user.last_name)

admin.site.register(Profile, ProfileAdmin)
