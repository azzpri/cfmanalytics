from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="profile")
    facebook_ads_account = models.CharField(verbose_name="Facebook Ads account ID", blank=True, max_length=64)
    facebook_ads = models.CharField(verbose_name="Facebook Ads ID", blank=True, max_length=64)