from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from .views import index, login_google, choose_facebook_account, \
    choose_facebook_ads

urlpatterns = [
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^admin/', admin.site.urls),
    url('', include('social_django.urls', namespace='social')),
    url(r'^$', index, name="index"),
    url(r'^login-google/', login_google, name="login_google"),
    url(r'^choose-facebook-account/', choose_facebook_account,
        name="choose-facebook-account"),
    url(r'^choose-facebook-ads/', choose_facebook_ads, name="choose-facebook-ads")

]
