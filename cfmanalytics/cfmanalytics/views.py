from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template import RequestContext
from facebookads.api import FacebookAdsApi
from facebookads.adobjects import user
from facebookads.adobjects.adaccount import AdAccount
from facebookads.adobjects.campaign import Campaign
from facebookads.exceptions import FacebookRequestError
from analytics.models import Profile

FacebookAdsApi.init(settings.SOCIAL_AUTH_FACEBOOK_KEY,
                    settings.SOCIAL_AUTH_FACEBOOK_SECRET,
                    settings.FACEBOOK_ACCESS_TOKEN)

@login_required
def choose_facebook_ads(request):
    ad_account = request.user.profile.facebook_ads_account
    if not ad_account:
        return redirect('/')
    else:
        try:
            sp = request.user.social_auth.get(provider="facebook")
        except:
            return redirect('/')
        ad_account = AdAccount(ad_account)
        campaigns = ad_account.get_campaigns(fields=[Campaign.Field.name])
        context = {'campaigns': campaigns}
        return render(request, "choose-facebook-ads.html", context)

@login_required
def choose_facebook_account(request):
    if request.method == 'POST' and \
                request.POST.get('ad-account') and request.user.profile:
        request.user.profile.facebook_ads_account = request.POST.get('ad-account')
        request.user.profile.save()
        return redirect('/choose-facebook-ads')
    else:
        try:
            sp = request.user.social_auth.get(provider="facebook")
        except:
            return redirect('/')
        if not request.user.profile.facebook_ads_account:
            facebook_user = user.User(fbid=sp.uid)
            try:
                facebook_ads_accounts = list(facebook_user.get_ad_accounts(
                    fields=[AdAccount.Field.name, AdAccount.Field.id]))
            except FacebookRequestError as e:
                return HttpResponse(e)
            context = {'facebook_ads_accounts': facebook_ads_accounts}
            return render(request, "choose-facebook-account.html", context)
        else:
            return redirect("/choose-facebook-ads")

@login_required
def login_google(request):
    return render(request, "registration/google.html", {})

@login_required
def index(request):
    social_profiles = request.user.social_auth.all()
    google_connected = False
    facebook_connected = False
    for profile in social_profiles:
        if profile.provider == "google-oauth2":
            google_connected = True
        elif profile.provider == 'facebook':
            facebook_connected = True
    if not facebook_connected:
        return redirect('/login')
    if not google_connected:
        return redirect('/login-google')
    else:
        profile = request.user.profile
        if not profile.facebook_ads_account:
            return redirect('/choose-facebook-account')
        elif not profile.facebook_ads:
            return redirect('/choose-facebook-ads')
        else:
            return render(request, "index.html", {})

def link_user_profile(backend, user, response, *args, **kwargs):
    ''' Create Profile for User if in not exist.
    '''
    if not hasattr(user, 'profile'):
        Profile.objects.create(user=user).save()

def unlink_user_profile(backend, user, *args, **kwargs):
    ''' Remove Profile for User if it exist.
    '''
    if hasattr(user, 'profile'):
        Profile.objects.get(user=user).delete()